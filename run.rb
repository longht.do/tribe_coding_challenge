#!/usr/bin/env ruby
lib = File.expand_path('../lib', __FILE__)
$:.unshift(lib)

require_relative 'lib/tribe'

usage = %Q(
------------------------------------ USAGE ------------------------------------
Command:         ./run.rb [FILE_PATH]
)

if ARGV.length == 1
  file_name = ARGV[0]
  Controller.new.execute(file_name)
else
  puts "\nWrong number of arguments\n"
  puts usage
end



