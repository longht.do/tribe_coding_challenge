RSpec.describe Input do
  describe '#read' do
    context 'example data file 1' do
      let(:file_name) { 'data/data1.txt' }
      let(:expected) do
        {
          image: 10,
          audio: 15,
          video: 17
        }
      end

      it 'returns quantity for each submission format code' do
        expect(Input.new.read(file_name)).to eq(expected)
      end
    end

    context 'example data file 2 with invalid format code' do
      let(:file_name) { 'data/data2.txt' }

      it 'raises an error' do
        expect { Input.new.read(file_name) }.to raise_error InvalidFormatCode
      end
    end
  end
end