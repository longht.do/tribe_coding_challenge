RSpec.describe Item do
  describe "#total_price" do
    subject { Item.new(bundle_size, quantity, unit_price).total_price }

    context 'bundles of 5, quantity => 1, unit price => 450' do
      let(:bundle_size) { 5 }
      let(:quantity) { 1 }
      let(:unit_price) { 450 }

      let(:expected)  { 450 }

      it 'returns total price' do
        expect(subject).to eq(expected)
      end
    end

    context 'bundles of 10, quantity => 2, unit price => 800' do
      let(:bundle_size) { 10 }
      let(:quantity) { 2 }
      let(:unit_price) { 800 }

      let(:expected)  { 1600 }

      it 'returns total price' do
        expect(subject).to eq(expected)
      end
    end

    context 'bundles of 10, quantity => 4, unit price => 800' do
      let(:bundle_size) { 10 }
      let(:quantity) { 4 }
      let(:unit_price) { 800 }

      let(:expected)  { 3200 }

      it 'returns total price' do
        expect(subject).to eq(expected)
      end
    end
  end
end