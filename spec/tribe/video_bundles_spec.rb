RSpec.describe VideoBundles do
  describe "#break_down" do
    subject { described_class.new.break_down(quantity) }
    let(:bundles_of_3) { 0 }
    let(:bundles_of_5) { 0 }
    let(:bundles_of_9) { 0 }
    let(:expected) do
      {
        3 => {
          quantity: bundles_of_3,
          unit_price: described_class::SMALL_BUNDLE_UNIT_PRICE
        },
        5 => {
          quantity: bundles_of_5,
          unit_price: described_class::MEDIUM_BUNDLE_UNIT_PRICE
        },
        9 => {
          quantity: bundles_of_9,
          unit_price: described_class::LARGE_BUNDLE_UNIT_PRICE
        }
      }
    end

    context '9 videos' do
      let(:quantity) { 9 }
      let(:bundles_of_9) { 1 }

      it 'returns 1 bundle of 9' do
        expect(subject).to eq(expected)
      end
    end

    context '5 videos' do
      let(:quantity) { 5 }
      let(:bundles_of_5) { 1 }

      it 'returns 1 bundle of 5' do
        expect(subject).to eq(expected)
      end
    end

    context '3 videos' do
      let(:quantity) { 3 }
      let(:bundles_of_3) { 1 }

      it 'returns 1 bundle of 3' do
        expect(subject).to eq(expected)
      end
    end

    context '17 videos' do
      let(:quantity) { 17 }
      let(:bundles_of_3) { 1 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_9) { 1 }

      it 'returns 1 bundle of 3, 1 bundle of 5 and 1 bundle of 9' do
        expect(subject).to eq(expected)
      end
    end

    context '6 videos' do
      let(:quantity) { 6 }
      let(:bundles_of_3) { 2 }

      it 'returns 2 bundles of 3' do
        expect(subject).to eq(expected)
      end
    end

    context '11 videos' do
      let(:quantity) { 11 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_3) { 2 }

      it 'returns 1 bundle of 5 and 2 bundles of 3' do
        expect(subject).to eq(expected)
      end
    end

    context '20 videos' do
      let(:quantity) { 20 }
      let(:bundles_of_3) { 2 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_9) { 1 }

      it 'returns 2 bundles of 3, 1 bundle of 5 and 1 bundle of 9' do
        expect(subject).to eq(expected)
      end
    end

    context '21 videos' do
      let(:quantity) { 21 }
      let(:bundles_of_3) { 1 }
      let(:bundles_of_9) { 2 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end

    context '22 videos' do
      let(:quantity) { 22 }
      let(:bundles_of_3) { 1 }
      let(:bundles_of_5) { 2 }
      let(:bundles_of_9) { 1 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end

    context '23 videos' do
      let(:quantity) { 23 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_9) { 2 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end

    context '24 videos' do
      let(:quantity) { 24 }
      let(:bundles_of_3) { 2 }
      let(:bundles_of_9) { 2 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end

    context '25 videos' do
      let(:quantity) { 25 }
      let(:bundles_of_3) { 2 }
      let(:bundles_of_5) { 2 }
      let(:bundles_of_9) { 1 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end

    context '26 videos' do
      let(:quantity) { 26 }
      let(:bundles_of_3) { 1 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_9) { 2 }

      it 'returns the correct result' do
        expect(subject).to eq(expected)
      end
    end
  end
end