RSpec.describe AudioBundles do
  describe '#break_down' do
    subject { described_class.new.break_down(quantity) }
    let(:bundles_of_3) { 0 }
    let(:bundles_of_6) { 0 }
    let(:bundles_of_9) { 0 }

    let(:expected) do
      {
        3 => {
          quantity: bundles_of_3,
          unit_price: described_class::SMALL_BUNDLE_UNIT_PRICE
        },
        6 => {
          quantity: bundles_of_6,
          unit_price: described_class::MEDIUM_BUNDLE_UNIT_PRICE
        },
        9 => {
          quantity: bundles_of_9,
          unit_price: described_class::LARGE_BUNDLE_UNIT_PRICE
        }
      }
    end

    context '3 audios' do
      let(:quantity) { 3 }
      let(:bundles_of_3) { 1 }


      it 'returns 1 bundle of 3' do
        expect(subject).to eq(expected)
      end
    end

    context '6 audios' do
      let(:quantity) { 6 }
      let(:bundles_of_6) { 1 }

      it 'returns 1 bundle of 6' do
        expect(subject).to eq(expected)
      end
    end

    context '9 audios' do
      let(:quantity) { 9 }
      let(:bundles_of_9) { 1 }

      it 'returns 1 bundle of 9' do
        expect(subject).to eq(expected)
      end
    end

    context '15 audios' do
      let(:quantity) { 15 }
      let(:bundles_of_6) { 1 }
      let(:bundles_of_9) { 1 }

      it 'returns 1 bundle of 9 and 1 bundle of 6' do
        expect(subject).to eq(expected)
      end
    end

    context '18 audios' do
      let(:quantity) { 18 }
      let(:bundles_of_9) { 2 }

      it 'returns 2 bundle of 9' do
        expect(subject).to eq(expected)
      end
    end
  end
end