RSpec.describe Output do
  describe "#summary" do
    subject { Output.new.summary(order) }

    context 'image' do
      let(:img_bundle_of_5) { Item.new(5, 1, 450) }
      let(:img_bundle_of_10) { Item.new(10, 2, 800) }
      let(:order) do
        {
          image: {
            total_quantity: 25,
            items: [img_bundle_of_5, img_bundle_of_10]
          }
        }
      end
      let(:expected) do
        "25 IMG $2050\n\t1 x 5 $450\n\t2 x 10 $1600\n"
      end

      it 'returns summary' do
        expect(subject).to eq(expected)
      end
    end

    context 'audio' do
      let(:audio_bundle_of_3) { Item.new(3, 0, 427.50) }
      let(:audio_bundle_of_6) { Item.new(6, 1, 810) }
      let(:audio_bundle_of_9) { Item.new(9, 4, 1147.50) }
      let(:order) do
        {
          audio: {
            total_quantity: 42,
            items: [audio_bundle_of_3, audio_bundle_of_6, audio_bundle_of_9]
          }
        }
      end
      let(:expected) do
        "42 FLAC $5400.0\n\t0 x 3 $0.0\n\t1 x 6 $810\n\t4 x 9 $4590.0\n"
      end

      it 'returns summary' do
        expect(subject).to eq(expected)
      end
    end

    context 'video' do
      let(:video_bundle_of_3) { Item.new(3, 1, 570) }
      let(:video_bundle_of_5) { Item.new(5, 1, 900) }
      let(:video_bundle_of_9) { Item.new(9, 1, 1530) }

      let(:order) do
        {
          video: {
            total_quantity: 17,
            items: [video_bundle_of_3, video_bundle_of_5, video_bundle_of_9]
          }
        }
      end
      let(:expected) do
        "17 VID $3000\n\t1 x 3 $570\n\t1 x 5 $900\n\t1 x 9 $1530\n"
      end

      it 'returns summary' do
        expect(subject).to eq(expected)
      end
    end

    context 'all formats' do
      let(:img_bundle_of_5) { Item.new(5, 1, 450) }
      let(:img_bundle_of_10) { Item.new(10, 2, 800) }
      let(:audio_bundle_of_3) { Item.new(3, 1, 427.50) }
      let(:audio_bundle_of_6) { Item.new(6, 2, 810) }
      let(:audio_bundle_of_9) { Item.new(9, 3, 1147.50) }
      let(:video_bundle_of_3) { Item.new(3, 1, 570) }
      let(:video_bundle_of_5) { Item.new(5, 1, 900) }
      let(:video_bundle_of_9) { Item.new(9, 1, 1530) }

      let(:order) do
        {
          image: {
            total_quantity: 25,
            items: [img_bundle_of_5, img_bundle_of_10]
          },
          audio: {
            total_quantity: 42,
            items: [audio_bundle_of_3, audio_bundle_of_6, audio_bundle_of_9]
          },
          video: {
            total_quantity: 17,
            items: [video_bundle_of_3, video_bundle_of_5, video_bundle_of_9]
          }
        }
      end

      let(:expected) do
        <<~SUMMARY
          25 IMG $2050\n\t1 x 5 $450\n\t2 x 10 $1600
          42 FLAC $5490.0\n\t1 x 3 $427.5\n\t2 x 6 $1620\n\t3 x 9 $3442.5
          17 VID $3000\n\t1 x 3 $570\n\t1 x 5 $900\n\t1 x 9 $1530
        SUMMARY
      end

      it 'returns summary' do
        expect(subject).to eq(expected)
      end
    end

  end
end