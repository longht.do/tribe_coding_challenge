RSpec.describe ImageBundles do
  describe '#break_down' do
    subject { described_class.new.break_down(quantity) }

    let(:bundles_of_5) { 0 }
    let(:bundles_of_10) { 0 }

    let(:expected) do
      {
        5 => {
          quantity: bundles_of_5,
          unit_price: described_class::SMALL_BUNDLE_UNIT_PRICE
        },
        10 => {
          quantity: bundles_of_10,
          unit_price: described_class::MEDIUM_BUNDLE_UNIT_PRICE
        },
      }
    end

    context '5 images' do
      let(:quantity) { 5 }
      let(:bundles_of_5) { 1 }

      it 'returns 1 bundle of 5' do
        expect(subject).to eq(expected)
      end
    end

    context '10 images' do
      let(:quantity) { 10 }
      let(:bundles_of_10) { 1 }

      it 'returns 1 bundle of 10' do
        expect(subject).to eq(expected)
      end
    end

    context '25 images' do
      let(:quantity) { 25 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_10) { 2 }

      it 'returns 2 bundles of 10 and 1 bundle of 5' do
        expect(subject).to eq(expected)
      end
    end

    context '35 images' do
      let(:quantity) { 35 }
      let(:bundles_of_5) { 1 }
      let(:bundles_of_10) { 3 }

      it 'returns 3 bundles of 10 and 1 bundle of 5' do
        expect(subject).to eq(expected)
      end
    end
  end
end