RSpec.describe Controller do
  let(:file_name) { 'data/data3.txt' }

  describe '#execute' do
    let(:controller) { described_class.new }
    let(:summary) { double('summary') }

    it 'calls a method to get the summary' do
      expect(controller).to receive(:get_summary).with(file_name)
      controller.execute(file_name)
    end

    it 'writes to STDOUT' do
      allow(controller).to receive(:get_summary).with(file_name) { summary }
      expect(STDOUT).to receive(:write).with(summary)
      controller.execute(file_name)
    end
  end

  describe '#get_summary' do
    subject {described_class.new.get_summary(file_name)}

    let(:expected) {
      <<~SUMMARY
        25 IMG $2050\n\t1 x 5 $450\n\t2 x 10 $1600
        42 FLAC $5400.0\n\t0 x 3 $0.0\n\t1 x 6 $810\n\t4 x 9 $4590.0
        17 VID $3000\n\t1 x 3 $570\n\t1 x 5 $900\n\t1 x 9 $1530
      SUMMARY
    }

    it 'returns summary' do
      expect(subject).to eq(expected)
    end
  end
end