require_relative "tribe/bundles"
require_relative "tribe/image_bundles"
require_relative "tribe/audio_bundles"
require_relative "tribe/video_bundles"
require_relative "tribe/item"
require_relative "tribe/input"
require_relative "tribe/output"
require_relative "tribe/controller"
