class Controller
  def execute(file_name)
    summary = get_summary(file_name)
    STDOUT.write(summary)
  end

  def get_summary(file_name)
    quantities = Input.new.read(file_name)

    image_quantity = quantities[:image]
    image_breakdown = ImageBundles.new.break_down(image_quantity)

    audio_quantity = quantities[:audio]
    audio_breakdown = AudioBundles.new.break_down(audio_quantity)

    video_quantity = quantities[:video]
    video_breakdown = VideoBundles.new.break_down(video_quantity)

    order = {
      image: image_order(image_quantity, image_breakdown),
      audio: audio_order(audio_quantity, audio_breakdown),
      video: video_order(video_quantity, video_breakdown)
    }

    Output.new.summary(order)
  end

  private

  def image_order(image_quantity, image_breakdown)
    small = ImageBundles::SMALL_BUNDLE_SIZE
    medium = ImageBundles::MEDIUM_BUNDLE_SIZE

    {
      total_quantity: image_quantity,
      items: [
        Item.new(small,
                 image_breakdown[small][:quantity],
                 image_breakdown[small][:unit_price]),
        Item.new(medium,
                 image_breakdown[medium][:quantity],
                 image_breakdown[medium][:unit_price])
      ]
    }
  end

  def audio_order(audio_quantity, audio_breakdown)
    suborder(audio_quantity, audio_breakdown, AudioBundles)
  end

  def video_order(video_quantity, video_breakdown)
    suborder(video_quantity, video_breakdown, VideoBundles)
  end

  def suborder(quantity, breakdown, bundles_class)
    small = bundles_class::SMALL_BUNDLE_SIZE
    medium = bundles_class::MEDIUM_BUNDLE_SIZE
    large = bundles_class::LARGE_BUNDLE_SIZE

    {
      total_quantity: quantity,
      items: [
        Item.new(small,
                  breakdown[small][:quantity],
                  breakdown[small][:unit_price]),
        Item.new(medium,
                  breakdown[medium][:quantity],
                  breakdown[medium][:unit_price]),
        Item.new(large,
                  breakdown[large][:quantity],
                  breakdown[large][:unit_price])
      ]
    }
  end
end