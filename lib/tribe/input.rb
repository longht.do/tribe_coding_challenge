class InvalidFormatCode < StandardError; end

class Input
  def read(file_name)
    line_items = parse(file_name)
    build_order(line_items)
  end

  private

  def parse(file_name)
    lines = []

    File.open(file_name, 'r') do |file|
      file.each_line { |line| lines << line.strip }
    end

    lines
  end

  def build_order(line_items)
    order = {}
    line_items.each do |line_item|
      quantity, format_code = get_quantity_and_format_code(line_item)

      case format_code
      when 'IMG'
        order[:image] = quantity
      when 'FLAC'
        order[:audio] = quantity
      when 'VID'
        order[:video] = quantity
      else
        raise InvalidFormatCode
      end
    end

    return order
  end

  def get_quantity_and_format_code(line_item)
    quantity_as_string, format_code = line_item.split(' ')
    [quantity_as_string.to_i, format_code]
  end
end