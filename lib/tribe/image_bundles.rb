class ImageBundles
  SMALL_BUNDLE_SIZE = 5
  MEDIUM_BUNDLE_SIZE = 10
  SMALL_BUNDLE_UNIT_PRICE = 450
  MEDIUM_BUNDLE_UNIT_PRICE = 800

  def break_down(quantity)
    bundles_of_10, remainder = quantity.divmod(MEDIUM_BUNDLE_SIZE)
    bundles_of_5 = remainder == 0 ? 0 : (remainder / SMALL_BUNDLE_SIZE)
    return {
      SMALL_BUNDLE_SIZE => {
        quantity: bundles_of_5,
        unit_price: SMALL_BUNDLE_UNIT_PRICE
      },
      MEDIUM_BUNDLE_SIZE => {
        quantity: bundles_of_10,
        unit_price: MEDIUM_BUNDLE_UNIT_PRICE
      }
    }
  end
end
