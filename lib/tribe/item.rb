class Item
  attr_accessor :bundle_size, :quantity

  def initialize(bundle_size, quantity, unit_price)
    @bundle_size = bundle_size
    @quantity = quantity
    @unit_price = unit_price
  end

  def total_price
    @quantity * @unit_price
  end
end

