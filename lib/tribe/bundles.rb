class Bundles
  def break_down(quantity)
    large_bundles, remainder = quantity.divmod(l_size)
    medium_bundles = small_bundles = nil

    while large_bundles >= 0 do
      m_and_s_quantity = quantity - (large_bundles * l_size)
      medium_bundles, small_bundles =
        break_down_medium_and_small_bundles(m_and_s_quantity)

      if small_bundles.nil?
        large_bundles -= 1
      else
        break
      end
    end

    return breakdown_builder(small_bundles, medium_bundles, large_bundles)
  end

  private

  def break_down_medium_and_small_bundles(m_and_s_quantity)
    return [0, 0] if m_and_s_quantity == 0

    medium_bundles = m_and_s_quantity / m_size
    small_bundles = nil

    while medium_bundles >= 0 do
      s_bundles_items_count = m_and_s_quantity - (medium_bundles * m_size)
      quotient, remainder = s_bundles_items_count.divmod(s_size)

      if remainder == 0
        small_bundles = quotient
        break
      else
        medium_bundles -= 1
      end
    end

    [medium_bundles, small_bundles]
  end

  def breakdown_builder(small_bundles, medium_bundles, large_bundles)
    return {
      s_size => {
        quantity: small_bundles,
        unit_price: s_unit_price
      },
      m_size => {
        quantity: medium_bundles,
        unit_price: m_unit_price
      },
      l_size => {
        quantity: large_bundles,
        unit_price: l_unit_price
      }
    }
  end

  def l_size
    self.class::LARGE_BUNDLE_SIZE
  end

  def m_size
    self.class::MEDIUM_BUNDLE_SIZE
  end

  def s_size
    self.class::SMALL_BUNDLE_SIZE
  end

  def l_unit_price
    self.class::LARGE_BUNDLE_UNIT_PRICE
  end

  def m_unit_price
    self.class::MEDIUM_BUNDLE_UNIT_PRICE
  end

  def s_unit_price
    self.class::SMALL_BUNDLE_UNIT_PRICE
  end
end