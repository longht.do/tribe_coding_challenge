class Output
  def summary(order)
    line_items(order, :image, 'IMG') + line_items(order, :audio, 'FLAC') + line_items(order, :video, 'VID')
  end

  private

  def line_items(order, format, code)
    sub_order = order[format]
    return '' if sub_order.nil?

    total_price = sub_order[:items].map(&:total_price).inject(0){|sum,x| sum + x }
    total_quantity = sub_order[:total_quantity]
    result = "#{total_quantity} #{code} $#{total_price}\n"

    sub_order[:items].each do |item|
      result << "\t#{item.quantity} x #{item.bundle_size} $#{item.total_price}\n"
    end

    result
  end
end