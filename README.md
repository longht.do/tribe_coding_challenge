# Tribe Coding Challenge
_Long Do <longht.do@gmail.com>_

## About

This application is developed using Ruby 2.7.2.

The purpose of this application is to give the social media influencer the best bundles deals when given a brand order.

## Setup

1. Clone this Repo
> $ git clone https://gitlab.com/longht.do/tribe_coding_challenge.git
2. Go to the root of the app
`cd tribe_coding_challenge`
3. Install the required gem
`bundle install`

## Usage

Command

`.run.rb [FILE_PATH]`

Examples

`./run.rb data/data1.txt`

`./run.rb data/data2.txt`

`./run.rb data/data3.txt`

`./run.rb data/data4.txt`

`./run.rb data/data5.txt`


## Test

Run the test suite

`bundle exec rspec`

## Implementation Notes

This application consists of 8 classes including `ImageBundles`, `AudioBundles`, `VideoBundles`, `Bundles`, `Item`, `Controller`, an input class, and an output class.

`ImageBundles`, `AudioBundles`, and `VideoBundles` will break down the given order into smaller bundles according to their bundle size. `AudioBundles` and `VideoBundles` have a lot of similarities so they inherit from the superclass `Bundles`. I decided that `ImageBundles` should not inherit from `Bundles` because the cons outweigh the pros. `ImageBundles` has 2 bundle sizes, whereas `AudioBundles` and `VideoBundles` have 3 bundle sizes. It will require some logic to deal with the difference in the number of sizes, which makes the code harder to follow. Therefore, I keep `ImageBundles` as its own class.

`Item` will work out the cost of 1 line item of the order when given the bundle size, quantity and unit price.

`Controller` class will integrate all the classes mentioned above and make them work together.

If given an invalid format code (e.g., PIC, PNG), it will raise an `InvalidFormatCode` error.

There is an assumption that there is always a perfect combination of bundles to make up the number of items that the brand is asking. For example, the brand will not require the influencers to submit 4 videos. The brand should ask for the number (of videos) such as 3, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, etc.

For those numbers that are not perfectly divisible, I would suggest the influencer to increase the number of items to the closest perfect number according to the bundle size they are asking. But for the purpose of this application, I think the implementation is complicated enough.